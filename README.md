# django

Django openclassrooms tutorial

Start the db :
```
$ source .env
$ docker run -d -v $(pwd)/pgdata:/var/lib/postgresql/data \
    -e POSTGRES_DB=$DB_NAME \
    -e POSTGRES_USER=$DB_USER \
    -e POSTGRES_PASSWORD=$DB_PASS \
    postgres
```
